public class PerMiles {
    public static void main(String[] args) {
        double miles = 100.2;
        final double KILOMETERS_PER_MILE = 1.609;
        double kilometers = 3;

        kilometers = kilometers*KILOMETERS_PER_MILE;

        System.out.printf("%.3f", kilometers);
    }
}
