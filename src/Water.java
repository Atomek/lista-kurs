import java.util.Scanner;

public class Water {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Temperature t0 t1 liters");
        double t0 = Double.parseDouble(scanner.next());
        double t1 = Double.parseDouble(scanner.next());
        double lit = Double.parseDouble(scanner.next());

        double jule = 4190*(t1-t0)*lit;
        System.out.printf("%.3f kJ",jule/1000 );
    }
}
