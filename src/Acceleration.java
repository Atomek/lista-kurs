import java.util.Scanner;

public class Acceleration {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("v_1, v_2, t_0");
        double v1 = Double.parseDouble(scanner.next());
        double v2 = Double.parseDouble(scanner.next());
        double t0 = Double.parseDouble(scanner.next());

        System.out.printf("%.4f", (v1-v2)/t0);
    }
}
