package HexagonShapely;

public class HexagonShapely {

    double sizeA;

    public HexagonShapely(double sizeA){
        this.sizeA=sizeA;
        System.out.println("Figure created");
    }

    public double getSizeA() {
        return sizeA;
    }

    public double getArea(){
        return  Math.pow(getSizeA(), 2)*3*Math.sqrt(3)/2;
    }

    public double getCircuit(){
        return getSizeA()*6;
    }

    public void getdiameters(){
        System.out.println("Długa średnica: " + 2*getSizeA() + "Ta druga " + getSizeA()*Math.sqrt(3));
    }

    public double getRad(){
        return Math.sqrt(3)*getSizeA()/2;
    }
}
