public class Array2DTo1D {
    public static void main(String[] args) {
        int sizeX = 5;
        int sizeY = 7;
        int[][] arrayWithNubers = createArray(sizeX, sizeY);

        final int RIGHT = 1;
        final int DOWN = 2;
        final int LEFT = 3;
        final int UP = 4;
        int kierunek = RIGHT;


        int w=0;
        int k=0;
        for(int i=0;i<=sizeX*sizeY;i++)
        {
            System.out.print(arrayWithNubers[w][k] + ", ");
            switch(kierunek)
            {
                case RIGHT:
                    if(k<sizeX-1 )
                        k++;
                    else
                    {
                        w++;
                        kierunek=DOWN;
                    }
                    break;
                case DOWN:
                    if(w<sizeX-1 )
                        w++;
                    else
                    {
                        k--;
                        kierunek=LEFT;
                    }
                    break;
                case LEFT:
                    if(k>0 )
                        k--;
                    else
                    {
                        w--;
                        kierunek=UP;
                    }
                    break;
                case UP:
                    if(w>0 )
                        w--;
                    else
                    {
                        k++;
                        kierunek=RIGHT;
                    }
                    break;
            }
        }


    }

    private static int[][] createArray(int sizeX, int sizeY) {
        int counter =1;
        int[][] arrayOfNumbers = new int[sizeX][sizeY];
        for (int i = 0; i < sizeX ; i++) {
            for (int j = 0; j < sizeY; j++) {
                arrayOfNumbers[i][j] = counter;
                counter++;
                System.out.printf("%4d", arrayOfNumbers[i][j]);
            }
            System.out.println();
        }
        return arrayOfNumbers;
    }
}
