import java.util.Scanner;

public class Distance {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("point x1 y1");
        int x1 = scanner.nextInt();
        int y1 = scanner.nextInt();
        System.out.println("point x2 y2");
        int x2 = scanner.nextInt();
        int y2 = scanner.nextInt();

        int d = (int) Math.sqrt(Math.pow(x2-x1, 2) + Math.pow(y2-y1, 2));

        System.out.println(d);
    }
}
