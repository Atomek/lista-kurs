import java.util.Random;
import java.util.Scanner;

public class Saper {
    static Scanner scanner = new Scanner(System.in);
    static Random random = new Random();
    public static void main(String[] args) {

        System.out.println("Podaj wymiar tablicy");
        int rows = scanner.nextInt();
        int columns = scanner.nextInt();

        System.out.println("Prawodopodobieństwo");
        int p = scanner.nextInt();

        Boolean[][] array = new Boolean[rows][columns];
        double prawdo;
        do {
            array[random.nextInt(rows)][random.nextInt(columns)]=true;
            prawdo=resultIt(array);
        }while(p>prawdo);





        String [][] straras = putStars(array, rows, columns);



        howManyStars(straras);
        printArray(straras);

    }

    private static String[][] howManyStars(String[][] straras) {

        for (int i = 1; i < straras.length-1; i++) {
            for (int j = 1; j <straras[i].length-1; j++) {
                int result = 0;
                if (straras[i-1][j] == "*"){
                    result++;
                }
                if (straras[i+1][j] == "*"){
                    result++;
                }
                if (straras[i][j+1] == "*"){
                    result++;
                }
                if (straras[i][j-1] == "*"){
                    result++;
                }
                if (straras[i-1][j-1] == "*"){
                    result++;
                }
                if (straras[i-1][j+1] == "*"){
                    result++;
                }
                if (straras[i+1][j-1] == "*"){
                    result++;
                }
                if (straras[i+1][j+1] == "*"){
                    result++;
                }
                straras[i][j]=Integer.toString(result);
            }
        }
        return straras;
    }

    private static void printArray(String[][] array) {
        for (int counterX = 0; counterX < array.length; counterX++) {
            for (int counterY = 0; counterY < array[counterX].length; counterY++) {
                System.out.printf("%6s",array[counterX][counterY] );
            }
            System.out.println();
        }
    }

    private static String[][] putStars(Boolean[][] array, int rows, int columns) {
        String[][] arrayWithStrings = new String[rows][columns];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j]!=null){
                    arrayWithStrings[i][j]="*";
                }
            }
        }
        return arrayWithStrings;
    }


    private static double resultIt(Boolean[][] array) {
        int result = 0;
        int size = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                size++;
                if (array[i][j] != null){
                    result++;
                }
            }
        }
        return result*100/size;
    }
}