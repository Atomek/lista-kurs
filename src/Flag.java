import javax.xml.transform.Source;
import java.util.Scanner;

public class Flag {

    public static void main(String[] args) {
        int sizeX = 50;
        int sizeY = 15;

        printFlag(putLineWithStars(createArray(sizeX, sizeY)));
    }

    private static void printFlag(String[][] flagToPrint) {
        for (int i = 0; i < flagToPrint.length; i++) {
            for (int j = 0; j < flagToPrint[i].length; j++) {
                System.out.print(flagToPrint[i][j]);
            }
            System.out.println();
        }
    }

    private static String[][] putLineWithStars(String[][] array) {
        for (int counterY = 0; counterY < array.length; counterY++) {
            if (counterY < 7) {
                if (counterY % 2 == 0) {
                    for (int counterX = 0; counterX < 12; counterX++) {
                        if (counterX % 2 == 0) {
                            array[counterY][counterX] = "*";
                        } else {
                            array[counterY][counterX] = " ";
                        }
                    }
                } else {
                    for (int counterX = 0; counterX < 12; counterX++) {
                        if (counterX % 2 == 0) {
                            array[counterY][counterX] = " ";
                        } else {
                            array[counterY][counterX] = "*";
                        }
                    }
                }
            } else {
                for (int i = 0; i < array.length; i++) {
                    for (int j = 0; j < array[i].length; j++) {
                        if (array[i][j] == null) {
                            array[i][j] = "=";
                        }
                    }
                }
            }

        }
        return array;
    }

    private static String[][] createArray(int sizeX, int sizeY) {
        String[][] flagArray = new String[sizeY][sizeX];
        return flagArray;
    }
}
