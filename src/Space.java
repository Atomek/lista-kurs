import java.util.Scanner;

public class Space {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {

        String string = scanner.nextLine();
        int spaces = 0;
        for (char ch :
                string.toCharArray()) {
            if(ch==' '){
                spaces++;
            }
        }
         
         System.out.println(spaces + "spaces");
    }
}
