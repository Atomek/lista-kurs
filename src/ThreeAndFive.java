public class ThreeAndFive {
    public static void main(String[] args) {
        System.out.println("3 and 5");
        for (int i = 1; i < 101; i++) {
            if (i%3==0 && i%5==0){
                System.out.print(i + ", ");
            }
        }
        System.out.println();
        System.out.println("only 3");
        for (int i = 1; i < 101; i++) {
            if (i%3==0){
                System.out.print(i + ", ");
            }
        }
        System.out.println();
        System.out.println("only 5");
        for (int i = 1; i < 101; i++) {
            if (i%5==0){
                System.out.print(i + ", ");
            }
        }
    }
}
